// import fetch from "node-fetch";
let boards = [];
let image;
function makeBoard(data) {
  console.log("in make");
  const board = document.querySelector(".boardCollection");
  const div = document.createElement("div");
  const a = document.createElement("a");
  a.href = "#";
  const img = document.createElement("img");
  if (data.prefs.backgroundImage) {
    img.setAttribute("src", data.prefs.backgroundImage);
  } else {
    if(!image){
      img.setAttribute(
        "src",
        "https://trello-backgrounds.s3.amazonaws.com/SharedBackground/original/ffc78a2df69512dbbef1e9e791740a99/photo-1630518615523-0d82e3985c06"
      );
    }
    else{
      img.setAttribute("src",image );
    }
  }
  img.style.height = "12vh";
  img.style.width = "20vh";
  const name = document.createElement("p");
  // name.style.textDecoration = ;
  name.style.fontWeight = "600px";

  name.style.color = "white";

  name.innerHTML = data.name;
  a.append(img);
  a.append(name);

  name.style.position = "relative";
  name.style.top = "-100px";
  div.append(a);
  board.append(div);
  div.addEventListener("click", () => {
    // console.log(data.url);
    localStorage.setItem("id",`${data.id}`);
   window.location = "board.html";
   
  });
}
function fetchApi() {
  fetch(
    "https://api.trello.com/1/members/me/boards?&key=312eb850997509c4c01a20047a10aedb&token=e0a3123d627aa97fde4550343ae8c4661d19dd81afdd5230268656f51093ab13"
  )
    .then((response) => response.json())
    .then((data) => {
      // console.log(data);
      boards = [...data];
      boards.forEach((element) => {
        console.log(element);
        makeBoard(element);
      });
    });
}
fetchApi();
const createBoard = document.querySelector(".createBoard");
createBoard.addEventListener("click", popUp);

function popUp() {
  const modal = document.querySelector(".modal");
  console.log("model ", modal);
  modal.style.display = "block";
  const createbtn = document.querySelector(".btn");
  const deletebtn = document.querySelector(".delete");

  deletebtn.addEventListener("click", (e) => {
    modal.style.display = "none";
  });
  createbtn.addEventListener("click", (e) => {
    
    const input = document.querySelector(".input");
    console.log("input data", input.value);
    createBoardFunction(input.value, modal);
  });
}

function createBoardFunction(name, modal) {
  console.log(name);
  fetch(
    `https://api.trello.com/1/boards/?name=${name}&key=312eb850997509c4c01a20047a10aedb&token=e0a3123d627aa97fde4550343ae8c4661d19dd81afdd5230268656f51093ab13`,
    {
      method: "POST",
    }
  )
    .then((response) => {
      console.log(`Response: ${response.status} ${response.statusText}`);
      //console.log(response.json())
      return response.json();
    })
    .then((text) => {
      console.log(text);
      text.prefs.backgroundImage = image;
      makeBoard(text);
      modal.style.display = "none";
    })
    .catch((err) => console.error(err));
}

/* 
fields=name,url&key=312eb850997509c4c01a20047a10aedb&token=e0a3123d627aa97fde4550343ae8c4661d19dd81afdd5230268656f51093ab13
*/
const color = document.querySelector(".color");
color.addEventListener("click",(e)=>{
  if(e.target.className !== "color"){
    console.log(e.target);
    const modal = document.querySelector(".model-top");
    console.log("modal",modal);
    if(e.target.tagName == "DIV"){
      image= e.target.className;
    modal.style.backgroundColor = e.target.className;
    }
    else{
      console.log(e.target.getAttribute("src"));
      image = e.target.getAttribute("src");
      modal.style.backgroundImage = `url(${e.target.getAttribute("src")})`;
      modal.style.backgroundSize = "cover";
    }

  }
  console.log(image);
  
})



