// This code sample uses the 'node-fetch' library:
// https://www.npmjs.com/package/node-fetch
function openNav() {
  document.getElementById("mySidebar").style.width = "250px";
  document.getElementById("main").style.marginLeft = "250px";
}

function closeNav() {
  document.getElementById("mySidebar").style.width = "0";
  document.getElementById("main").style.marginLeft = "0";
}
console.log(localStorage.getItem("id"));
function getlist(id) {
  fetch(
    `https://api.trello.com/1/boards/${id}/lists?key=312eb850997509c4c01a20047a10aedb&token=e0a3123d627aa97fde4550343ae8c4661d19dd81afdd5230268656f51093ab13`,
    {
      method: "GET",
      headers: {
        Accept: "application/json",
      },
    }
  )
    .then((response) => {
      console.log(`Response: ${response.status} ${response.statusText}`);
      return response.json();
    })
    .then((lists) => {
      console.log(lists);
      lists.forEach((list) => {
        
        
        fetch(`https://api.trello.com/1/lists/${list.id}/cards?key=312eb850997509c4c01a20047a10aedb&token=e0a3123d627aa97fde4550343ae8c4661d19dd81afdd5230268656f51093ab13`, {
          method: 'GET',
          headers: {
            'Accept': 'application/json'
          }
        })
          .then(response => {
            console.log(
              `Response: ${response.status} ${response.statusText}`
            );
            return response.json();
          })
          .then(cards => {
            console.log(cards);
           createList(list,cards);
             
    
          })
          .catch(err => console.error(err));
      });
    })
    .catch((err) => console.error(err));
}

getlist(localStorage.getItem("id"));

function createList(list,cards) {
  console.log(cards)
  
  if(cards !== []){
    var fragment = new DocumentFragment();
    const div = document.createElement("DIV");
    div.setAttribute("id",list.id)
    const p = document.createElement("p");
    p.innerHTML = `${list.name}`;
    div.append(p);
    cards.forEach(eachCard => {
      console.log (eachCard.name)
      const card = document.createElement("DIV");
      const p1 = document.createElement("p");
      p1.innerHTML = `${eachCard.name}`;
      card.append(p1);
      card.style.border = "1px solid black"
      fragment.append(card);
     
    });
    div.append(fragment);

    const div2 = document.createElement("DIV");
    div2.classList.add("addCard");
    const input = document.createElement("input");
    input.setAttribute("type","text");
    input.setAttribute("value","");
   
    const btn = document.createElement("button");
    btn.append("button");
    btn.addEventListener("click",createNewCard)
    div2.append(input);
    div2.append(btn)
   

    div.append(div2);
    const addList = document.querySelector(".addList"); 
    const listContainer = document.querySelector(".list");
    listContainer.insertBefore(div,addList);
  
  }
  else{
    const div = document.createElement("DIV");
    const addList = document.querySelector(".addList"); 
  
    const p = document.createElement("p");

    p.innerHTML = `${list.name}`;
    div.append(p);

    const listContainer = document.querySelector(".list");
    listContainer.insertBefore(div,addList);

  }
}
const addListButton = document.querySelector(".addListButton");
addListButton.addEventListener("click", addNewList);



function addNewList(e) {
    const name = document.querySelector(".input");

    console.log(name.value);
  fetch(`https://api.trello.com/1/boards/${localStorage.getItem("id")}/lists?name=${name.value}&key=312eb850997509c4c01a20047a10aedb&token=e0a3123d627aa97fde4550343ae8c4661d19dd81afdd5230268656f51093ab13`, {
    method: "POST",
    headers: {
      Accept: "application/json",
    },
  })
    .then((response) => {
      console.log(`Response: ${response.status} ${response.statusText}`);
      return response.json();
    })
    .then((list) => 
    {
        createList(list);
    })
    .catch((err) => console.error(err));
}
function createNewCard(e){
  console.log("new card",e.target.parentElement.parentElement);
  const list = e.target.parentElement.parentElement;
  const createDiv = e.target.parentElement;
  fetch(`https://api.trello.com/1/cards?idList=${list.getAttribute("id")}&key=312eb850997509c4c01a20047a10aedb&token=e0a3123d627aa97fde4550343ae8c4661d19dd81afdd5230268656f51093ab13`, {
  method: 'POST',
  headers: {
    'Accept': 'application/json'
  }
})
  .then(response => {
    console.log(
      `Response: ${response.status} ${response.statusText}`
    );
    return response.text();
  })
  .then(text => console.log(text))
  .catch(err => console.error(err));

}